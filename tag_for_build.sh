#!/bin/bash
# This script will tag the necessary repositories for Synapsoft builds.

function usage
{
    echo "usage: tag_for_build [-f] [-d clone_to_directory] [-b branch_name] [-td tag_description] tag"
    echo "			[-f]				Will move tag if it already exists"
    echo "			[-d clone_to_directory ] 	Defaults to './tagbuilds'"
    echo "			[-b branch_name]		Defaults to 'master'"
    echo "			[-td tag description]		Example: 'Build for SZ 8.0.0 S##'"
    echo "			tag				Example: 'Aireo/7.0.0_B##'"
}

BASE_DIR="./tagbuilds"
BRANCH="master"
TAG=
TAG_DESC=
MOVE_TAG=

while [ "$1" != "" ]; do
    case $1 in
        -f | --force )          MOVE_TAG="-f"
                                ;;
        -d )    				shift
                                BASE_DIR=$1
                                ;;
        -b )    				shift
                                BRANCH=$1
                                ;;
        -td )    				shift
                                TAG_DESC=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     TAG=$1
    esac
    shift
done

GIT_URL="ssh://git.synapsense.int/git"
#GIT_URL="ssh://172.30.100.26/git"
REPOS=('middle' 'deploymentlab' 'webapps' 'shadoww' 'shadowwcat' 'control_appliance')

#echo "BASE_DIR = $BASE_DIR"
#echo "BRANCH = $BRANCH"
#echo "TAG = $TAG"
#echo "TAG_DESC = $TAG_DESC"
#echo "MOVE_TAG = $MOVE_TAG"

if [[ ! -d "$BASE_DIR" ]]; then
	mkdir "$BASE_DIR"
fi

cd "$BASE_DIR"

for repo in "${REPOS[@]}"; do
	echo ""
	echo "EVALUATING $repo"
	
	REPO_URL=
	if [ $repo = "mobileapps" ] && [ $BRANCH = "master" ]; then
		echo "'$repo' not included in Aireo or later"
	elif [ $repo = "shadowwcat" ] && [ $BRANCH != "master" ]; then
		echo "'$repo' only in Aireo or later"
	elif [ $repo = "control_appliance" ] && [ $BRANCH != "master" ]; then
		echo "'$repo' only in Aireo or later"
	elif [ $repo = "deploymentlab" ]; then
		REPO_URL="$GIT_URL/tools/$repo.git"
	else
		REPO_URL="$GIT_URL/$repo.git"
	fi
	
	if [ -n "$REPO_URL" ]; then
		if [ ! -d "$repo" ]; then
			echo "CLONING $repo to $BASE_DIR/$repo"
			git clone "$REPO_URL"
		fi

		if [ -d "$repo" ]; then
			pushd "$repo" > /dev/null

			echo "CHECKING OUT $BRANCH branch of $repo"
			git checkout $BRANCH
			git pull

			echo "REMOVING untracked files"
			git clean -d -n -x -q
			
			echo "TAGGING $BRANCH with $TAG"
			git tag $MOVE_TAG -a "$TAG" -m "$TAG_DESC"
			git push $MOVE_TAG origin "$TAG"

			popd > /dev/null
		fi
	fi

done
