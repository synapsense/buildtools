/* Attempts to sign data */

def signtool = """signtool.exe sign /v /t "http://timestamp.verisign.com/scripts/timestamp.dll" """

def p = ~/.*\.exe/

def signclos

signclos =  {
    f ->
	//println "Scanning " + f
	f.eachDir( signclos );
    f.eachFileMatch(p) {
		file -> 
		def stf = signtool + " " + file
		println("Signing " + stf)
		def proc = stf.execute()
		proc.waitFor()
		println proc.in.text.trim()
		println proc.err.text.trim()
	}
}

signclos(new File('.'));