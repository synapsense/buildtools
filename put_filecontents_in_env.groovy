import hudson.model.*

def ws = build.getWorkspace().toString()
def filename = build.getWorkspace().toString() + "/webconsole/changes.html"
def webconsole_changes = new File(filename).getText('UTF-8')
filename = build.getWorkspace().toString() + "/middle/changes.html"
def middle_changes = new File(filename).getText('UTF-8')
filename = build.getWorkspace().toString() + "/mapsense/changes.html"
def mapsense_changes = new File(filename).getText('UTF-8')
filename = build.getWorkspace().toString() + "/shadowwcat/changes.html"
def shadowwcat_changes = new File(filename).getText('UTF-8')
def pa = new ParametersAction([
  new StringParameterValue("MIDDLE_CHANGES", middle_changes),
  new StringParameterValue("MAPSENSE_CHANGES", mapsense_changes),
  new StringParameterValue("WEBCONSOLE_CHANGES", webconsole_changes),
  new StringParameterValue("SHADOWWCAT_CHANGES", shadowwcat_changes)
])

// add variable to current job
Thread.currentThread().executable.addAction(pa)